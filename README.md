# Vue Storybook

## What is it

A tool to create components in an environment isolated from the business logic project. With an isolated environment, we assure that the component is reusable and is not influenced by other stylesheets in the project.

## Benefits

With this separated environment, we can access different contexts for that component. For instance, when we want a component with a primary style, large text and at a certain language, we have to search for it through the system. One way out would be to create a screen just for testing and call the component we want to test, passing mocked data.
With storybooks, we can just access the component and style it the way that we want.

#### Examples:

- [Live example](https://5ccbc373887ca40020446347-ihnlmpbzos.chromatic.com/index.html?path=/story/colors--page)

[Example](https://storybook.js.org/f818682edbbcdf2c04093f633aa36761/example-browse-all-stories-optimized.mp4)

[Example](https://storybook.js.org/946b2f4bdb006e8475d21202d68b9eec/addons-walkthrough-optimized.mp4)

[Example](https://storybook.js.org/214deb06b9b97c2fdb370298bbcdd65c/addon-measure-optimized.mp4)

[Example](https://lh3.googleusercontent.com/HWOx8bBhagi2AwxAufNupB0sjsEog1zti_bd3t4qvXTaekEuA6V8jffxCgoWRpnnDym0P2dzRlPjyb2cXKysR4QmwKwTH9JqrUu4N8abXajc3bXOI5c8aoMQ1VO4LzeZ2a0sEJxj)

[Example](https://storybookblog.ghost.io/content/images/2021/08/6.4-interaction-testing-hero-square.gif)

In short: you will have an environment to create your components in isolation, be able to test them and it also ends up serving as documentation for your components library. It is customizable and has several plugins that can facilitate various tasks, such as testing in different resolutions, changing the theme, testing accessibility, internationalization, etc.

## Cons

